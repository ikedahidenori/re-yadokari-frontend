var ua = navigator.userAgent;
var scrollTag;
if( ua.indexOf('OPR') !== -1 || ua.indexOf('Edge') !== -1 ) {
  scrollTag = 'body';
} else {
  scrollTag = ( !window.chrome && 'WebkitAppearance' in document.documentElement.style )? 'body' : 'html';
}

$(document).ready(function(){

    $("#topSlider .slick").slick({
        arrows: true,
        centerMode: true,
        centerPadding: "22%",
        autoplay: true,
        autoplaySpeed: 6000,
        responsive: [{
            breakpoint: 1919,
            settings: {
                centerPadding: "20%"
            }
        }, {
            breakpoint: 1599,
            settings: {
                centerPadding: "18%"
            }
        }, {
            breakpoint: 1139,
            settings: {
                centerPadding: "12%"
            }
        }, {
            breakpoint: 1023,
            settings: {
                centerPadding: "15%"
            }
        }, {
            breakpoint: 767,
            settings: {
                centerPadding: "0"
            }
        }],
        
	    dots: true,
	    customPaging: function(slider, i) {
	      return '<span></span>';
	    }
    })
    $('#topSlider .slick').on('init', function(event, slick, direction){
	  $('#topSlider').removeClass('loading');
	});
	
/*
    $("#topQuoteKeywordsWrapper .quote").slick({
        arrows: false,
        centerMode: false,
        autoplay: true,
        autoplaySpeed: 6000,
	    dots: false,
	    fade: true
    });
*/
    
    //追従サイドバー
    //permalinkページなら
    $('#main.permalink').each(function()
    {
		var $footer = $('#footer'),
			$side   = $('.postMeta');
		
		function update() {
		
			// grab the latest scroll position
		    var y	= $(window).scrollTop(),
		    	vh	= $(window).height(),
		    	fy 	= $footer.offset().top;
		    
		    
		    //まだfooterの上
		    if( y + vh < fy )
		    {
			    $side.removeClass('isReachedToFooter');
		    }
		    
		    //footerに到達
		    else
		    {
			    $side.addClass('isReachedToFooter');
		    }
		
			// keep going
		    requestAnimationFrame(update);
		}
		
		// schedule up the start
		window.addEventListener('scroll', update, false);
	    
    });
    
    
   	//ヘッダー 
    $('#header').each(function()
    {	    
	    var $header = $(this),
	    	$footer = $('#footer');
		var y0 = $(window).scrollTop();
		
		function update() {
		
			// grab the latest scroll position
		    var y	= $(window).scrollTop(),
		    	h   = $header.height(),
		    	vh	= $(window).height(),
		    	fy 	= $footer.offset().top;
		    
		    if( y > 200 )
		    {
				  $header.addClass('shadow');
			}
			
			else
			{
				  $header.removeClass('shadow');
			}
			
			
		    if( y > y0 )
		    {
			    if( y >= 200) {
				  $header.addClass('minus');
			    }
		    }
		    
		    else
		    {
			    $header.removeClass('minus');
		    }
		
			y0 = y;
			
		}
		
		// schedule up the start
		window.addEventListener('scroll', update, false);
	    
    });
    

    //Smooth Scroll
    //var Jump = new Jump();

	//MASONRY
    $('.masonry').each(function()
    {
		$(this).masonry();
    });
    
    //EM-DASH
    $('.body:contains("——")').each(function()
    {
        var txt = $(this).html();
         $(this).html(
             txt.replace(/——/g,'<span class="emdash">——</span>')
		);
    });
    
    
	  //scroll to top
	  var $scrollTopTop = $('#toTop');  
	  $scrollTopTop
	    .hide()
	    .on('click',function () 
	    {
	      $(scrollTag).animate({scrollTop:0},"300");
	    });
	    
	    var $pagination = $('#fake'),
	        $window = $(window),
	        $document = $(document),
	        $body = $('body'),
	        $footer = $('#colophon');
	    $(window).scroll(function () 
	    {
	      var y = $window.scrollTop() + $window.height(),
	          Y = $document.height() - $pagination.height();
	          
	        if( y > 1100 ) 
	          $scrollTopTop.fadeIn();
	        else
	          $scrollTopTop.fadeOut();
	        
	        if( y < Y ) 
	          $body.removeClass('reachedToBottom');
	        else
	          $body.addClass('reachedToBottom');
	    });
    
});


